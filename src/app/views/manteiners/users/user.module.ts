import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import {UserListComponent} from './user-list/user-list.component';
import {UserRoutingModule} from './user-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  declarations: [
    UserListComponent
  ]
})
export class UserModule {
}
