import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CriticalListComponent} from './critical-list/critical-list.component';


export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'critical-list',
        component: CriticalListComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriticalRoutingModule {
}
