import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: `
    <router-outlet></router-outlet>
    <ngx-spinner
      bdColor="rgba(51,51,51,0.7)"
      size="medium"
      color="#F7B40E"
      type="ball-clip-rotate-pulse">
      <p style="color: white"> Procesando... </p>
    </ngx-spinner>`
})
export class AppComponent implements OnInit {
  constructor(private router: Router) {
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });
  }
}
