import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {DailyListComponent} from './daily-list/daily-list.component';


export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'daily-list',
        component: DailyListComponent,
      }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailyRoutingModule {
}
