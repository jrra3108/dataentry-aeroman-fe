import {ItfResponse} from '../../../../interfaces/itf-response';

export interface ItfCriticalList {
  response: ItfResponse;
  data: {
    critical_list: [ItfCritical];
  };
}

export interface ItfCritical {
  id?: string;
  aircraft: string;
  workOrder: string;
  criticalPath: string;
  tatImpact: string;
  mitigationPlan: string;
  responsible: string;
  status: string;
  statusFinal: string;
}
