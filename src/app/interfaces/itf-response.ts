export interface ItfResponse {
  code: number;
  description: string;
}
