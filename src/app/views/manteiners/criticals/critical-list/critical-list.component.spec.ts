import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriticalListComponent } from './usuarios-lista.component';

describe('UsuariosListaComponent', () => {
  let component: CriticalListComponent;
  let fixture: ComponentFixture<CriticalListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriticalListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriticalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
