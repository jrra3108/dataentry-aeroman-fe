import { NgModule } from '@angular/core';
import {ManteinersRoutingModule} from './manteiners-routing.module';

@NgModule({
  imports: [
    ManteinersRoutingModule
  ],
  declarations: []
})
export class ManteinersModule {
}
