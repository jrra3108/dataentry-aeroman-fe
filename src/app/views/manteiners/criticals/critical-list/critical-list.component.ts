import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {ItfCritical} from '../interface/itf-critical';
import {ApiAeroService} from '../../../../services/api/api-aero.service';
import {PaginationInstance} from 'ngx-pagination';

@Component({
  selector: 'app-critical-list',
  templateUrl: './critical-list.component.html',
  styleUrls: ['./critical-list.component.scss']
})
export class CriticalListComponent implements OnInit {

  public TITLE = 'Critical Paths';
  public criticalList: [ItfCritical];
  public typeAction: number;
  public titleModal: string;
  public modalRef: BsModalRef;
  public valueSearch: string;
  public criticalForm: FormGroup;
  public criticalEdit: ItfCritical;
  public idCriticalDelete: string;

  public configPage: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(
    private apiAero: ApiAeroService,
    private modalService: BsModalService,
    private fb: FormBuilder,
    private toatr: ToastrService
  ) {
  }

  ngOnInit() {
    this.initForm();
    this.getListaCriticals();
  }

  public onPageChange(number: number) {
    this.configPage.currentPage = number;
  }

  public searchCriterial() {
    if (this.valueSearch !== '' && this.valueSearch !== null) {
      this.apiAero.findCritical(this.valueSearch).subscribe(resp => {
        if (resp.response.code === 0) {
          this.criticalList = resp.data.critical_list;
          // @ts-ignore
          if (this.criticalList.length === 0) {
            this.toatr.success('No se encontraron registros', this.TITLE);
          }
        } else {
          this.toatr.error(resp.response.description);
        }
      });
      this.valueSearch = '';
    } else {
      this.toatr.warning('Debe ingresar un valor de búsqueda', this.TITLE);
    }
  }

  public getListaCriticals() {
    this.apiAero.listCritical().subscribe(resp => {
      if (resp.response.code === 0) {
        this.criticalList = resp.data.critical_list;
        // @ts-ignore
        if (this.criticalList.length === 0) {
          this.toatr.success('No se encontraron registros', this.TITLE);
        }
      } else {
        this.toatr.error(resp.response.description);
      }
    });
  }

  private initForm() {
    this.criticalForm = this.fb.group({
      aircraft: ['', Validators.compose([Validators.required])],
      workOrder: ['', Validators.compose([Validators.required])],
      criticalPath: ['', Validators.compose([Validators.required])],
      tatImpact: ['', Validators.compose([Validators.required])],
      mitigationPlan: ['', Validators.compose([Validators.required])],
      responsible: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([Validators.required])],
      statusFinal: ['', Validators.compose([Validators.required])],
    });
  }

  public openModal(type, template, crit) {
    this.typeAction = type;
    console.log(type);
    switch (type) {
      case 1 :
        this.titleModal = 'Agregar Critical Path';
        break;
      case 2 :
        this.criticalEdit = crit;
        this.titleModal = 'Editar Critical Path';
        this.criticalForm.controls.aircraft.setValue(this.criticalEdit.aircraft);
        this.criticalForm.controls.workOrder.setValue(this.criticalEdit.workOrder);
        this.criticalForm.controls.criticalPath.setValue(this.criticalEdit.criticalPath);
        this.criticalForm.controls.tatImpact.setValue(this.criticalEdit.tatImpact);
        this.criticalForm.controls.mitigationPlan.setValue(this.criticalEdit.mitigationPlan);
        this.criticalForm.controls.responsible.setValue(this.criticalEdit.responsible);
        this.criticalForm.controls.status.setValue(this.criticalEdit.status);
        this.criticalForm.controls.statusFinal.setValue(this.criticalEdit.statusFinal);
        break;
    }

    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary modal-lg',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public executeAction() {
    switch (this.typeAction) {
      case 1:
        this.agregar();
        break;
      case 2:
        this.editar();
        break;
    }
  }

  public agregar() {
    if (this.criticalForm.valid) {
      const body = {
        aircraft: this.criticalForm.getRawValue().aircraft,
        workOrder: this.criticalForm.getRawValue().workOrder,
        criticalPath: this.criticalForm.getRawValue().criticalPath,
        tatImpact: this.criticalForm.getRawValue().tatImpact,
        mitigationPlan: this.criticalForm.getRawValue().mitigationPlan,
        responsible: this.criticalForm.getRawValue().responsible,
        status: this.criticalForm.getRawValue().status,
        statusFinal: this.criticalForm.getRawValue().statusFinal,
      };

      this.apiAero.createCritical(body).subscribe(resp => {
        switch (resp.code) {
          case 0:
            this.modalRef.hide();
            this.getListaCriticals();
            this.toatr.success('Critical Path creado con Éxito', this.TITLE);
            this.criticalForm.reset();
            break;
          default:
            this.toatr.error(resp.description, this.TITLE);
            break;
        }
      });
    } else {
      this.toatr.warning('Complete todos los campos', this.TITLE);
    }
  }

  public editar() {
    if (this.criticalForm.valid) {
      const body = {
        id: this.criticalEdit.id,
        aircraft: this.criticalForm.getRawValue().aircraft,
        workOrder: this.criticalForm.getRawValue().workOrder,
        criticalPath: this.criticalForm.getRawValue().criticalPath,
        tatImpact: this.criticalForm.getRawValue().tatImpact,
        mitigationPlan: this.criticalForm.getRawValue().mitigationPlan,
        responsible: this.criticalForm.getRawValue().responsible,
        status: this.criticalForm.getRawValue().status,
        statusFinal: this.criticalForm.getRawValue().statusFinal,
      };

      this.apiAero.updateCritical(body).subscribe(resp => {
        switch (resp.code) {
          case 0:
            this.modalRef.hide();
            this.getListaCriticals();
            this.toatr.success('Critical Path actualizado con Éxito', this.TITLE);
            this.criticalForm.reset();
            break;
          default:
            this.toatr.error(resp.description, this.TITLE);
            break;
        }
      });
    } else {
      this.toatr.warning('Complete todos los campos', this.TITLE);
    }
  }

  public confirmar(id, template) {
    this.idCriticalDelete = id;
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public eliminar() {
    this.apiAero.deleteCritical(this.idCriticalDelete).subscribe( resp => {
      switch (resp.code) {
        case 0:
          this.modalRef.hide();
          this.getListaCriticals();
          this.toatr.success('Critical Path eliminado con Éxito', this.TITLE);
          break;
        default:
          this.toatr.error(resp.description, this.TITLE);
          break;
      }
    });
  }

  public closeModal() {
    this.modalRef.hide();
    this.criticalForm.reset();
  }

}
