import {ItfResponse} from '../../../../interfaces/itf-response';


export interface ItfDailyList {
  response: ItfResponse;
  data: {
    daily_list: [ItfDaily];
  };
}

export interface ItfDaily {
  id?: string;
  aircraft: string;
  workOrder: string;
  task: string;
  complete: number;
  reason: string;
  dateId: Date;
}
