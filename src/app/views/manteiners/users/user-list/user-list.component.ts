import {Component, OnInit} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PaginationInstance} from 'ngx-pagination';
import {ApiAeroService} from '../../../../services/api/api-aero.service';
import {ToastrService} from 'ngx-toastr';
import {ItfUser} from '../interface/itf-user';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit {

  public TITLE = 'Usuarios';
  public listUser: [ItfUser];
  public typeAction: number;
  public titleModal: string;
  public modalRef: BsModalRef;
  public valueSearch: string;
  public userForm: FormGroup;
  public userEdit: ItfUser;
  public idUserDelete: string;

  public configPage: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(
    private fb: FormBuilder,
    private apiAero: ApiAeroService,
    private toatr: ToastrService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    this.initForm();
    this.getListaUsers();
  }

  public onPageChange(number: number) {
    this.configPage.currentPage = number;
  }

  public searchUser() {
    if (this.valueSearch !== '' && this.valueSearch !== null) {
      this.apiAero.findUser(this.valueSearch).subscribe(resp => {
        if (resp.response.code === 0) {
          this.listUser = resp.data.lista_usuarios;
          // @ts-ignore
          if (this.listDaily.length === 0) {
            this.toatr.success('No se encontraron registros', this.TITLE);
          }
        } else {
          this.toatr.error(resp.response.description);
        }
      });
      this.valueSearch = '';
    } else {
      this.toatr.warning('Debe ingresar un valor de búsqueda', this.TITLE);
    }
  }

  private initForm() {
    this.userForm = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([Validators.required])],
    });
  }

  public getListaUsers() {
    this.apiAero.listUser().subscribe( resp => {
      if (resp.response.code === 0) {
        this.listUser = resp.data.lista_usuarios;
        // @ts-ignore
        if (this.listUser.length === 0) {
          this.toatr.success('No se encontraron registros', this.TITLE);
        }
      } else {
        this.toatr.error(resp.response.description);
      }
    });
  }

  public openModal(type, template, user) {
    this.typeAction = type;
    console.log(type);
    switch (type) {
      case 1 :
        this.titleModal = 'Agregar Daily Focus';
        break;
      case 2 :
        this.userEdit = user;
        this.titleModal = 'Editar Daily Focus';
        this.userForm.controls.username.setValue(this.userEdit.username);
        this.userForm.controls.status.setValue(this.userEdit.status);
        break;
    }

    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary modal-lg',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public executeAction() {
    switch (this.typeAction) {
      case 1:
        this.agregar();
        break;
      case 2:
        this.editar();
        break;
    }
  }

  public agregar() {
    if (this.userForm.valid) {
      const body = {
        username: this.userForm.getRawValue().username,
        password: this.userForm.getRawValue().password,
        status: this.userForm.getRawValue().status,
      };
      this.apiAero.createUser(body).subscribe( resp => {
        switch (resp.code) {
          case 0:
            this.modalRef.hide();
            this.getListaUsers();
            this.userForm.reset();
            this.toatr.success('Usuario Creado con Éxito', this.TITLE);
            break;
          default:
            this.toatr.error(resp.description, this.TITLE);
            break;
        }
      });
    } else {
      this.toatr.warning('Complete toda la información requerida', this.TITLE);
    }
  }

  public editar() {
    if (this.userForm.valid) {
      const body = {
        id: this.userEdit.id,
        username: this.userForm.getRawValue().username,
        password: this.userForm.getRawValue().password,
        status: this.userForm.getRawValue().status,
      };

      this.apiAero.updateUser(body).subscribe(resp => {
        switch (resp.code) {
          case 0:
            this.modalRef.hide();
            this.getListaUsers();
            this.toatr.success('Usuario actualizado con Éxito', this.TITLE);
            this.userForm.reset();
            break;
          default:
            this.toatr.error(resp.description, this.TITLE);
            break;
        }
      });
    } else {
      this.toatr.warning('Complete todos los campos', this.TITLE);
    }
  }

  public confirmar(id, template) {
    this.idUserDelete = id;
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public eliminar() {
    this.apiAero.deleteUser(this.idUserDelete).subscribe( resp => {
      switch (resp.code) {
        case 0:
          this.modalRef.hide();
          this.getListaUsers();
          this.toatr.success('Usuario eliminado con Éxito', this.TITLE);
          break;
        default:
          this.toatr.error(resp.description, this.TITLE);
          break;
      }
    });
  }

  public closeModal() {
    this.modalRef.hide();
    this.userForm.reset();
  }

}
