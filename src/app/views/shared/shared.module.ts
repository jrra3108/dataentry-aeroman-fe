import {ConfirmComponentComponent} from './components';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';


const COMPONENT_SHARED = [
    ConfirmComponentComponent
];

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule
    ],
    declarations: [
        ...COMPONENT_SHARED
    ],
    exports: [
        ...COMPONENT_SHARED
    ],
    entryComponents: [
        ConfirmComponentComponent
    ]
})

export class SharedModule {
}
