import {ItfResponse} from '../../../../interfaces/itf-response';


export interface ItfUserList {
  response: ItfResponse;
  data: {
    lista_usuarios: [ItfUser];
  };
}

export interface ItfUser {
  id?: string;
  username: string;
  password: string;
  status: string;
}
