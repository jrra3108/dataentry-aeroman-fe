import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs';


@Component({
  selector: 'app-confirm-component',
  templateUrl: './confirm-component.component.html',
  styleUrls: ['./confirm-component.component.scss']
})
export class ConfirmComponentComponent implements OnInit {

  public onClose: Subject<Object>;

  constructor() { }

  ngOnInit() {
    this.onClose = new Subject();
  }

  public onConfirm(): void {
    this.onClose.next(true);
  }

  public onCancel(): void {
    this.onClose.next(false);
  }

}
