import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DailyRoutingModule} from './daily-routing.module';
import {DailyListComponent} from './daily-list/daily-list.component';
import {NgxPaginationModule} from 'ngx-pagination';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DailyRoutingModule,
    NgxPaginationModule
  ],
  declarations: [
    DailyListComponent
  ]
})
export class DailyModule {
}
