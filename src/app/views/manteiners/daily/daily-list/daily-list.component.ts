import { Component, OnInit } from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {ItfDaily} from '../interface/itf-daily';
import {ApiAeroService} from '../../../../services/api/api-aero.service';
import {PaginationInstance} from 'ngx-pagination';
import * as moment from 'moment';


@Component({
  selector: 'app-daily-list',
  templateUrl: './daily-list.component.html',
  styleUrls: ['./daily-list.component.scss']
})
export class DailyListComponent implements OnInit {

  public TITLE = 'Daily Focus';
  public listDaily: [ItfDaily];
  public typeAction: number;
  public titleModal: string;
  public modalRef: BsModalRef;
  public valueSearch: string;
  public dailyForm: FormGroup;
  public dailyEdit: ItfDaily;
  public idDailyDelete: string;

  public configPage: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(
    private fb: FormBuilder,
    private apiAero: ApiAeroService,
    private toatr: ToastrService,
    private modalService: BsModalService,
  ) { }

  ngOnInit() {
    this.initForm();
    this.getListaDaily();
  }

  public onPageChange(number: number) {
    this.configPage.currentPage = number;
  }

  public date(date) {
    return moment(date).format('DD-MM-YYYY');
  }

  public searchDaily() {
    if (this.valueSearch !== '' && this.valueSearch !== null) {
      this.apiAero.findDaily(this.valueSearch).subscribe(resp => {
        if (resp.response.code === 0) {
          this.listDaily = resp.data.daily_list;
          // @ts-ignore
          if (this.listDaily.length === 0) {
            this.toatr.success('No se encontraron registros', this.TITLE);
          }
        } else {
          this.toatr.error(resp.response.description);
        }
      });
      this.valueSearch = '';
    } else {
      this.toatr.warning('Debe ingresar un valor de búsqueda', this.TITLE);
    }
  }

  private initForm() {
    this.dailyForm = this.fb.group({
      aircraft: ['', Validators.compose([Validators.required])],
      workOrder: ['', Validators.compose([Validators.required])],
      task: ['', Validators.compose([Validators.required])],
      complete: ['', Validators.compose([Validators.required])],
      reason: ['', Validators.compose([Validators.required])],
      dateId: ['', Validators.compose([Validators.required])],
    });
  }

  public getListaDaily() {
    this.apiAero.listDaily().subscribe( resp => {
      if (resp.response.code === 0) {
        this.listDaily = resp.data.daily_list;
        // @ts-ignore
        if (this.listDaily.length === 0) {
          this.toatr.success('No se encontraron registros', this.TITLE);
        }
      } else {
        this.toatr.error(resp.response.description);
      }
    });
  }

  public openModal(type, template, dai) {
    this.typeAction = type;
    console.log(type);
    switch (type) {
      case 1 :
        this.titleModal = 'Agregar Daily Focus';
        break;
      case 2 :
        this.dailyEdit = dai;
        this.titleModal = 'Editar Daily Focus';
        this.dailyForm.controls.aircraft.setValue(this.dailyEdit.aircraft);
        this.dailyForm.controls.workOrder.setValue(this.dailyEdit.workOrder);
        this.dailyForm.controls.task.setValue(this.dailyEdit.task);
        this.dailyForm.controls.complete.setValue(this.dailyEdit.complete);
        this.dailyForm.controls.reason.setValue(this.dailyEdit.reason);
        this.dailyForm.controls.dateId.setValue(this.dailyEdit.dateId);
        break;
    }

    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary modal-lg',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public executeAction() {
    switch (this.typeAction) {
      case 1:
        this.agregar();
        break;
      case 2:
        this.editar();
        break;
    }
  }

  public agregar() {
    console.log(this.dailyForm.valid);
    if (this.dailyForm.valid) {
      const body = {
        aircraft: this.dailyForm.getRawValue().aircraft,
        workOrder: this.dailyForm.getRawValue().workOrder,
        task: this.dailyForm.getRawValue().task,
        complete: this.dailyForm.getRawValue().complete,
        reason: this.dailyForm.getRawValue().reason,
        dateId: this.dailyForm.getRawValue().dateId,
      };
      this.apiAero.createDaily(body).subscribe( resp => {
        switch (resp.code) {
          case 0:
            this.modalRef.hide();
            this.getListaDaily();
            this.dailyForm.reset();
            this.toatr.success('Daily Creado con Éxito', this.TITLE);
            break;
          default:
            this.toatr.error(resp.description, this.TITLE);
            break;
        }
      });
    } else {
      this.toatr.warning('Complete toda la información requerida', this.TITLE);
    }
  }

  public editar() {
    if (this.dailyForm.valid) {
      const body = {
        id: this.dailyEdit.id,
        aircraft: this.dailyForm.getRawValue().aircraft,
        workOrder: this.dailyForm.getRawValue().workOrder,
        task: this.dailyForm.getRawValue().task,
        complete: this.dailyForm.getRawValue().complete,
        reason: this.dailyForm.getRawValue().reason,
        dateId: this.dailyForm.getRawValue().dateId,
      };

      this.apiAero.updateDaily(body).subscribe(resp => {
        switch (resp.code) {
          case 0:
            this.modalRef.hide();
            this.getListaDaily();
            this.toatr.success('Daily actualizado con Éxito', this.TITLE);
            this.dailyForm.reset();
            break;
          default:
            this.toatr.error(resp.description, this.TITLE);
            break;
        }
      });
    } else {
      this.toatr.warning('Complete todos los campos', this.TITLE);
    }
  }

  public confirmar(id, template) {
    this.idDailyDelete = id;
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public eliminar() {
    this.apiAero.deleteDaily(this.idDailyDelete).subscribe( resp => {
      switch (resp.code) {
        case 0:
          this.modalRef.hide();
          this.getListaDaily();
          this.toatr.success('Daily Focus eliminado con Éxito', this.TITLE);
          break;
        default:
          this.toatr.error(resp.description, this.TITLE);
          break;
      }
    });
  }

  public closeModal() {
    this.modalRef.hide();
    this.dailyForm.reset();
  }

}
