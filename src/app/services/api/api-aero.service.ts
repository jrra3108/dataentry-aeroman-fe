import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {Observable} from 'rxjs';
import {ItfResponse} from '../../interfaces/itf-response';
import {finalize} from 'rxjs/operators';
import {ItfCriticalList} from '../../views/manteiners/criticals/interface/itf-critical';
import {ItfDailyList} from '../../views/manteiners/daily/interface/itf-daily';
import {ItfUserList} from '../../views/manteiners/users/interface/itf-user';


@Injectable()
export class ApiAeroService {

  private urlApi = environment.apiUrl + '/api/v1';

  constructor(
    private http: HttpClient,
    private spinner: NgxSpinnerService
  ) {}

  public hideSpinner() {
    setTimeout(() =>
        this.spinner.hide(),
      1000);
  }

  // Critical

  public createCritical(body): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.post<ItfResponse>( `${this.urlApi}/manteiners/critical`, body ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public updateCritical(body): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.put<ItfResponse>( `${this.urlApi}/manteiners/critical`, body ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public deleteCritical(id): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.delete<ItfResponse>( `${this.urlApi}/manteiners/critical/${id}` ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public listCritical(): Observable<ItfCriticalList> {
    this.spinner.show();
    return this.http.get<ItfCriticalList>( `${this.urlApi}/manteiners/critical`).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public findCritical(value: string): Observable<ItfCriticalList> {
    this.spinner.show();
    return this.http.get<ItfCriticalList>( `${this.urlApi}/manteiners/critical/find/${value}`).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  // Daily
  public createDaily(body): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.post<ItfResponse>( `${this.urlApi}/manteiners/daily`, body ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public updateDaily(body): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.put<ItfResponse>( `${this.urlApi}/manteiners/daily`, body ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public deleteDaily(id): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.delete<ItfResponse>( `${this.urlApi}/manteiners/daily/${id}` ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public listDaily(): Observable<ItfDailyList> {
    this.spinner.show();
    return this.http.get<ItfDailyList>( `${this.urlApi}/manteiners/daily`).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public findDaily(value: string): Observable<ItfDailyList> {
    this.spinner.show();
    return this.http.get<ItfDailyList>( `${this.urlApi}/manteiners/daily/find/${value}`).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  // Users
  public createUser(body): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.post<ItfResponse>( `${this.urlApi}/seguridad/usuarios`, body ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public updateUser(body): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.put<ItfResponse>( `${this.urlApi}/seguridad/usuarios`, body ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public deleteUser(id): Observable<ItfResponse> {
    this.spinner.show();
    return this.http.delete<ItfResponse>( `${this.urlApi}/seguridad/usuarios/${id}` ).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public listUser(): Observable<ItfUserList> {
    this.spinner.show();
    return this.http.get<ItfUserList>( `${this.urlApi}/seguridad/usuarios`).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }

  public findUser(value: string): Observable<ItfUserList> {
    this.spinner.show();
    return this.http.get<ItfUserList>( `${this.urlApi}/seguridad/usuarios/find/${value}`).pipe(finalize( () => {
      this.hideSpinner();
    }));
  }
}
