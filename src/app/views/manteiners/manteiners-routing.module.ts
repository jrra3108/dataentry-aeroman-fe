import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';


export const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'critical',
        loadChildren: () => import('./criticals/critical.module').then(m => m.CriticalModule),
        data: {
          title: 'Mantenimiento Critical Paths'
        }
      },
      {
        path: 'daily',
        loadChildren: () => import('./daily/daily.module').then(m => m.DailyModule),
        data: {
          title: 'Mantenimiento Daily Focus'
        }
      },
      {
        path: 'user',
        loadChildren: () => import('./users/user.module').then(m => m.UserModule),
        data: {
          title: 'Mantenimiento Usuarios'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManteinersRoutingModule {
}
