import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CriticalRoutingModule} from './critical-routing.module';
import {CriticalListComponent} from './critical-list/critical-list.component';
import {NgxPaginationModule} from 'ngx-pagination';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CriticalRoutingModule,
    NgxPaginationModule,
  ],
  declarations: [
    CriticalListComponent
  ]
})
export class CriticalModule {}
