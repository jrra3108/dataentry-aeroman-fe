interface NavAttributes {
  [propName: string]: any;
}

interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}

interface NavBadge {
  text: string;
  variant: string;
}

interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Inicio',
    url: '/app/dashboard',
    icon: 'fa fa-home',
  },
  {
    title: true,
    name: 'Seguridad'
  },
  {
    name: 'Usuarios',
    url: '/app/manteiners/user/user-list',
    icon: 'fa fa-user-circle'
  },
  {
    title: true,
    name: 'Mantenimientos'
  },
  {
    name: 'Critical Paths',
    url: '/app/manteiners/critical/critical-list',
    icon: 'fa fa-list'
  },
  {
    name: 'Daily Focus',
    url: '/app/manteiners/daily/daily-list',
    icon: 'fa fa-list'
  }
];
