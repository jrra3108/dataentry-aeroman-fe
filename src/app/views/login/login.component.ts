import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  public username = '';
  public password = '';

  constructor(
    private router: Router,
    private toatr: ToastrService,
  ) {}

  public login() {
    console.log(this.username);
    console.log(this.password);
    if (this.username === 'SuperUser' && this.password === 'aeroman') {
      this.router.navigate(['app/dashboard']);
    } else {
      this.toatr.error('Usuario y contrseña invalido');
    }
  }
}
